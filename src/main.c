/*
 * File name:       main.c
 * Author:          Joseilton Ferreira
 * Created:         01-nov-2020
 * Last modified:   09-abr-2021
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

int main(int argc, char* argv[]) {

  if (argc != 2) {
    printf("Input Data Error\n");
    exit(0);
  }

  // open file
  FILE* file;
  file = fopen(argv[1], "r");
 
  if (file == NULL) {
    perror("Error opening file");
    fclose(file);
    exit(0);
  }

  // variables
  int key;
  int line = 2;
  int erro = FALSE;
  char lat_p[16];
  char lat_n[16];
  char lon_p[16];
  char lon_n[16];

  // get first line information of "latitude and longitude" of input data
  fgets(lat_p, 16, file);
  fgetc(file);
  fgets(lon_p, 16, file);
  fgetc(file);

  // get second line information of "latitude and longitude" of input data
  fgets(lat_n, 16, file);
  fgetc(file);
  fgets(lon_n, 16, file);

  // compare initial variables for determine comparing key for the data
  if (strcmp(lat_p, lat_n) == 0) { 
    key = TRUE;
  } else if (strcmp(lon_p, lon_n) == 0) {
      key = FALSE;
  } else {
      printf("Find error in line %d\n", line);
      erro = TRUE;
  }

  // go through the input data comparing "latitude and longitude" alterning comparing key
  while (fgetc(file) != EOF) {
    strcpy(lat_p, lat_n);
    strcpy(lon_p, lon_n);
    fgets(lat_n, 16, file);
    fgetc(file);
    fgets(lon_n, 16, file);
    line++;

    if (key == TRUE) {
      if (strcmp(lon_p, lon_n) == 0) {      
        key = FALSE;
      } else {
        printf("Find error in line %d\n", line);
        //printf("%s\n", lon_p);
        erro = TRUE;
      }

    } else {
      if (strcmp(lat_p, lat_n) == 0) {
        key = TRUE;
      } else {
        printf("Find error in line %d\n", line);
        erro = TRUE;
      }
    }
  }

  // no errors msg
  if (erro == FALSE) {
    printf("No errors find in polygonal\n");
  }

  fclose(file);
  return 0;
}
