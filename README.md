# Polygonal Checker

## Description

Program to check polygonal for mineral requirements in **ANM - Agência Nacional de Mineração**.

## Data input format

The data file must contain latitude and longitude information on each line, as show below.

```shell
-;018;56;45;002;-;056;34;11;567
-;018;56;45;002;-;056;34;12;345
-;018;56;20;540;-;056;34;12;345
-;018;56;20;540;-;056;34;11;567
-;018;56;45;002;-;056;34;11;567
```
